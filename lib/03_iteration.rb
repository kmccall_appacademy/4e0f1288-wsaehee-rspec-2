# ### Factors
def factors(num)
  factors = []
  i = 1
  while i <= num
    factors << i if num % i == 0
    i += 1
  end
  factors
end

# ### Bubble Sort
class Array
  def bubble_sort!(&blk)
    if block_given?
      self.sort!(&blk)
    else
      self.sort! do |x, y|
        x <=> y
      end
    end
  end

  def bubble_sort(&prc)
    copy = self.dup
    copy.bubble_sort!
  end
end

# ### Substrings and Subwords
#
# Write a method, `substrings`, that will take a `String` and return an
# array containing each of its substrings. Don't repeat substrings.
# Example output: `substrings("cat") => ["c", "ca", "cat", "a", "at",
# "t"]`.
#
# Your `substrings` method returns many strings that are not true English
# words. Let's write a new method, `subwords`, which will call
# `substrings`, filtering it to return only valid words. To do this,
# `subwords` will accept both a string and a dictionary (an array of
# words).
def any_repeats?(arr_of_substr, word)
  result = arr_of_substr.any? do |substr|
    substr == word
  end
  result
end

def substrings(string)
  substrs = []
  first_idx = 0
  while first_idx < string.length
    second_idx = first_idx
    while second_idx < string.length
      substr = string[first_idx..second_idx]
      unless any_repeats?(substrs, substr)
        substrs << substr
      end
      second_idx += 1
    end
    first_idx += 1
  end
  substrs
end

def subwords(word, dictionary)
  words = substrings(word)
  valid_words = []
  words.each do |w|
    dictionary.each do |d|
      valid_words << w if w == d
    end
  end
  valid_words
end

# ### Doubler
def doubler(array)
  times_two = array.collect do |int|
    int * 2
  end
end

# ### My Each
class Array
  def my_each(&prc)
    i = 0
    while i < self.length
      prc.call(self[i])
      i += 1
    end
    self
  end
end

# ### My Enumerable Methods
class Array
  def my_map(&prc)
    results = []
    i = 0
    while i < self.length
      results << prc.call(self[i])
      i += 1
    end
    results
  end

  def my_select(&prc)
    results = []
    i = 0
    while i < self.length
      results << self[i] if prc.call(self[i])
      i += 1
    end
    results
  end

  def my_inject(&blk)
    acc ||= self[0]
    i = 1
    while i < self.length
      acc = blk.call(acc, self[i])
      i += 1
    end
  end
end

# ### Concatenate
def concatenate(strings)
  strings.inject {|sentence, word| sentence + "#{word}"}
end
